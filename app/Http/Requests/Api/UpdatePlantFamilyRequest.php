<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\PlantFamilyRequest;

class UpdatePlantFamilyRequest extends PlantFamilyRequest
{
    use ApiRequestTrait;
}
