<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Map my garden Documentation",
 *      description="API for plant management",
 *      @OA\Contact(
 *          email="erwann@duclos.xyz"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="MMG API Server"
 * )

    *
    * @OA\Tag(
    *     name="Map my garden",
    *     description="API Endpoints of Map my garden"
    * )
    */
class Controller extends BaseController
{
    
    use AuthorizesRequests, ValidatesRequests;
}
